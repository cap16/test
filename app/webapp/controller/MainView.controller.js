sap.ui.define(["de/bit/recap/UI5Auth0/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("de.bit.recap.UI5Auth0.controller.MainView", {

        _auth01: null,

        onInit: async function() {
        },

        doLogin: async function(oEvent) {
            console.log("doLogin");
            console.log(this.getOwnerComponent()._auth0);
            
            const isAuthenticated = await this.getOwnerComponent()._auth0.isAuthenticated();
            console.log(isAuthenticated);

            if (!isAuthenticated) {
                const login = async function() {
                    await this.getOwnerComponent()._auth0.loginWithRedirect({
                        redirect_uri: window.location.origin + "/webapp/index.html"
                      });
                }.bind(this);
                login();
            }
        },

        doLogout: async function(oEvent) {
            this.getOwnerComponent()._auth0.logout({
                returnTo: window.location.origin + "/webapp/index.html"
              });
        },

        doCAP: function() {
            console.log("doCap");
            this.navTo("cap", {}, true);
        }
    });
});
