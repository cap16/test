sap.ui.define([
    "de/bit/recap/UI5Auth0/controller/BaseController",
    "sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
    "use strict";

    return Controller.extend("de.bit.recap.UI5Auth0.controller.cap", {

        onInit: function() {

            var oViewModel = new JSONModel({
                showFooter: false,
                showAdd: true,
                busy: false
            });
            this.setModel(oViewModel, "capView");
        },


        onUpdateFinished: function(oEvent) {
            console.log("onUpdateFinished");

            // check if in edit mode
            var bEdit = this.getModel("capView").getProperty("/showFooter");
            if (bEdit) {
                var oList = this.byId("projectList");

                oList.getItems()[0].getCells()[0].setEnabled(true);
                oList.getItems()[0].getCells()[1].setEnabled(true);
                oList.getItems()[0].getCells()[2].setEnabled(true);
            }

            
        },

        onAddProject: function() {

            this.getModel("capView").setProperty("/showFooter", true);
            this.getModel("capView").setProperty("/showAdd", false);

            var oList = this.byId("projectList");
			var oBinding = oList.getBinding("items");

			var oContext = oBinding.create({
                "name" : "",
                "language" : "",
                "repository" : ""
            });

            if (oList.getItems()[0]) {
                oList.getItems()[0].getCells()[0].setEnabled(false);
                oList.getItems()[0].getCells()[1].setEnabled(false);
                oList.getItems()[0].getCells()[2].setEnabled(false);
            }
            

//           var sContextPath = oContext.getPath();
//            console.log(sContextPath);
/*
            oList.getItems().some(function (oItem) {

                var sItemPath = oItem.getBindingContextPath();
                var sContextPath = oContext.getPath();

                console.log(sItemPath);
                console.log(sContextPath);

				if (oItem.getBindingContext() === oContext) {
					oItem.focus();
					oItem.setSelected(true);
                    console.log(oItem);
					return true;
				}
			});
*/
        },

        doSave: function () {

            this.getModel("capView").setProperty("/showFooter", false);
            this.getModel("capView").setProperty("/showAdd", true);

            var oList = this.byId("projectList");
            oList.getItems()[0].getCells()[0].setEnabled(false);
            oList.getItems()[0].getCells()[1].setEnabled(false);
            oList.getItems()[0].getCells()[2].setEnabled(false);

		},


        _setBusy : function (bIsBusy) {
			var oModel = this.getView().getModel("capView");
			//oModel.setProperty("/busy", bIsBusy);
		}

    });
});
