sap.ui.define(
    ["sap/ui/core/UIComponent", 
    "sap/ui/Device", 
    "de/bit/recap/UI5Auth0/model/models",
    "sap/base/util/UriParameters",
],    
    function (UIComponent, Device, models, UriParameters, ) {
        "use strict";

        return UIComponent.extend("de.bit.recap.UI5Auth0.Component", {

            _auth0: null,

            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: async function () {
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // enable routing
                this.getRouter().initialize();

                // set the device model
                this.setModel(models.createDeviceModel(), "device");

                this.setModel(models.createUserModel(), "user");
                
                await this.configOIDCClient();

                console.log(this._auth0);

                var sCodeParam = UriParameters.fromQuery(window.location.search).get("code");
                var sStateParam = UriParameters.fromQuery(window.location.search).get("state");
                console.log(sCodeParam);
                console.log(sStateParam);

                if (sCodeParam && sStateParam) {

                    

                    //var oRouter = this.getRouter();
                    //oRouter.navTo("RouteMainView", {}, true);



                    await this._auth0.handleRedirectCallback();
                    var oUser = await this._auth0.getUser();
                    var oJwt = await this._auth0.getTokenSilently();

                    console.log(oUser);
                    console.log(oJwt);

                    // reset URL params                    
                    window.history.pushState('', 'UI5Auth0', '/webapp/index.html');
                }

                await this.checkOIDCParams();
                
            },

            configOIDCClient: async function() {

                const fetchAuthConfig = () => fetch("./auth_config.json");
                const configureClient = async function() {
                    const response = await fetchAuthConfig();
                    const config = await response.json();
                  
                    this._auth0 = await createAuth0Client({
                      domain: config.domain,
                      client_id: config.clientId,
                      redirect_uri: window.location.origin + "/webapp/index.html",
                      responseType: config.responseType,
                      audience: config.audience,
                      scope: config.scope

                    });  
                    console.log("auth0 client configured");


                }.bind(this);
                console.log("Start -- Client configured");
                await configureClient();
                console.log("End -- Client configured");

            },

            checkOIDCParams: async function() {

                const isAuthenticated =  await this._auth0.isAuthenticated();
                console.log(isAuthenticated);

                if (isAuthenticated) {
                    var oModel = this.getModel("user");

                    oModel.setProperty("/authenticated", true);
                    oModel.setProperty("/token", await this._auth0.getTokenSilently());
                    oModel.setProperty("/user", await await await this._auth0.getUser());

                    console.log(oModel.getData());

                    this._loadCapModel();

                    const claims = await this._auth0.getIdTokenClaims();
                    console.log(claims);

                }

            },


            _loadCapModel: function() {
                var oUserModel = this.getModel("user");
                var oModel = new sap.ui.model.odata.v4.ODataModel({
                    serviceUrl: "/public/",                    
                    httpHeaders: {
                        "Authorization": "Bearer " + oUserModel.getProperty("/token")
                    },
                    synchronizationMode: "None",
                    groupId: "$direct",
                    operationMode: "Server"
                });
                console.log(oModel);
                this.setModel(oModel, "projects");

            }
        });
    }
);
