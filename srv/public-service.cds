using db from '../db/data-model';

service PublicService {
    @requires: 'authenticated-user'
    entity Projects as projection on db.Projects;
}