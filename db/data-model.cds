using { cuid, User, managed } from '@sap/cds/common';
namespace db;

entity Projects: cuid, managed {
  name : String(150);
  language: String(150);
  repository: String;
}

annotate Projects with {
  createdBy  @cds.on.insert: $user;
}
